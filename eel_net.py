#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
r.neighbors --overwrite --verbose input=elevation output=test_rn \
    weighting_function=exponential weighting_factor=12

r.stream.extract --overwrite --verbose elevation=dem_1m_carved@p_eel_test \
    threshold=5000 stream_length=100 memory=100000 stream_raster=dem_1m_carved_streams \
    stream_vector=dem_1m_carved_streams direction=dem_1m_carved_fdir


1) identify eel lakes
2) route eel presence downstreams
3) create possible habitat quality predictor variables
 - altitude
 - temperature
 - flow accumulation
 - SPI
 - distance from sea
 - lake upstreams

4) create additional connectivity variables
 - number of culverts downstreams
 - number of road crossings downstreams
 - stream slope downstreams (avg, sqrt, 3rd_quart, 90perc, max)

5) collect data in pixels
 - landscape: all streams
 - occurrence: pixels downstreams from eel occurrences

6) run maxent (with and without connectivity)
7) tune maxent models  (with and without connectivity)

MASK@p_Agder_aal
r.cost -n --overwrite --verbose input=MASK@p_Agder_aal output=hav_cost_dist \
    outdir=upstreams_dir start_raster=Norway_FKB_Hav@p_eel_test memory=100000
/home/stefan/.grass7/addons/scripts/r.out.maxent_swd.py --overwrite --verbose \
    env_maps=elevation@PERMANENT,geology_30m@PERMANENT \
    alias_names=altitude,geology bgr_output=$HOME/bgr.swd \
    alias_output=$HOME/alias.csv species_masks=tmp_vc3Oq6x7JHM2_patch_258@stefan \
    species_output=$HOME/species species_names=patch258

"""

# import os
import itertools
from io import BytesIO

import sqlite3
from igraph import Graph
import numpy as np

# Set hardcoded GRASSBIN until grass-session 0.2 is released
# if not os.environ.get('GRASSBIN'):
#    os.environ['GRASSBIN'] = '/usr/local/bin/grass79'
from grass_session import Session
# from grass.script import encode


# Identify clusters
# for cluster identify end poinds (degree == 1)
# Get altitude
# Identify lowest point with degree == 1 and get epath to outlet for each of them
# for idx, edge in enum(epath) if epath[idx + 1] in g.adjacent(edge.target, mode="OUT")


# gscript.run_command('v.overlay', flags='t', overwrite=True, verbose=True,
#                     ainput='DTM_1m_streams_cleaned@p_eel_FKB_N502',
#                     atype='line', binput='FKB_Hav@p_eel_FKB_N502', # alayer='1',
#                     operator='not', output='DTM_1m_streams_cleaned_clipped',
#                     snap='0.01')

# Norway_N50_ArealdekkeFlate@p_eel_FKB_N50
occurrences = "eel"
limit_to_occ = False
streams = "dem_1m_carved_streams_cleaned"
lakes = "Norway_FKB_Vann_innsjoe@p_eel_FKB_N50"
roads = "N50_Barriers@p_eel_FKB_N50"
#N50_roads_org = "N50_roads@p_eel_FKB_N50"
#N50_bane_org = "N50_Bane@p_eel_FKB_N50"

fkb_barriers_pol = [
    "Norway_FKB_TraktorvegSti_lines",
    "Norway_FKB_Veg_lines",
    "Norway_FKB_Veg_polygons",
    "Norway_FKB_Bane_lines",
]
fkb_barriers = ["Norway_FKB_TraktorvegSti_lines_np", "Norway_FKB_Bane_lines"]
n50_barriers = ["Norway_N50_VegSti", "Norway_N50_Bane"]

NVE_barriers = ["Norway_NVE_Vannkraft_DamLinje", "Norway_NVE_Vannkraft_DamPunkt", "Norway_NVE_Vannkraft_Vannkraftverk"]
NVE_snaplines = ["DamLinje_lines", "DamPunkt_lines", "Vannkraftverk_lines"]

barriers = fkb_barriers  # n50_barriers +  # N50 kartdata introduserer mer feil enn det forbedrer resultat
barriers_pol = "Norway_FKB_Veg_polygons"
fkb_org = [
    "FKB_roads@p_eel_FKB_N50",
    "FKB_gravelroads@p_eel_FKB_N50",
    "FKB_Bane@p_eel_FKB_N50",
]


source_mapsets = ["p_eel_FKB_N50", "p_eel_elevation", "g_Hydrography", "p_Agder_aal"]

required_in_mapset = ["eel"]

lines = "lines"
culverts_nvdb = "lines"
elevation = "dem_1m_carved"
full_network = "{}_full_network".format(streams)

grassdb = "/home/shared/grassdata"
location = "ETRS_33N"
mapset = "p_Agder_aal_net_2"
output = "{}_net".format(streams)

## Read Network data from vector map
scales = [3, 9, 21, 31, 51]
input = "TEST4"
node_layer = "2"
arc_layer = "3"
table = "{}_{}".format(full_network, node_layer)
table_arcs = "{}_{}".format(full_network, arc_layer)


def lwa(val, length):
    """Compute length weighted average"""
    if np.any(~np.isnan(val)):
        weighted_average = sum(val[~np.isnan(val)] * length[~np.isnan(val)]) / sum(
            length[~np.isnan(val)]
        )
    else:
        weighted_average = None
    return weighted_average


def item_check(item):
    """Check item type to assign proper SQL types to columns"""
    if item:
        if np.issubdtype(type(item), np.number):
            if np.issubdtype(type(item), np.int8) or \
               np.issubdtype(type(item), np.int16) or \
               np.issubdtype(type(item), np.int32) or \
               np.issubdtype(type(item), np.int64) or \
               np.issubdtype(type(item), np.intp) or \
               np.issubdtype(type(item), np.uint8) or \
               np.issubdtype(type(item), np.uint16) or \
               np.issubdtype(type(item), np.uint32) or \
               np.issubdtype(type(item), np.uint64) or \
               np.issubdtype(type(item), np.uintp):
                item_type = "int"
            elif np.issubdtype(type(item), np.float_) or \
                 np.issubdtype(type(item), np.float32) or \
                 np.issubdtype(type(item), np.float64) or \
                 np.issubdtype(type(item), np.complex64) or \
                 np.issubdtype(type(item), np.complex128) or \
                 np.issubdtype(type(item), np.complex_):
                item_type = "float"
        else:
            item_type = "other"
    else:
        item_type = "None"
    return item_type


def snap_join(river_network=streams,
              snap_objects=occurrences,
              tmp_name=lines,
              snap_type="point",
              dmax=25,
              column="g_occurrenceid"):
    """Function that snaps points or lines to river networks
    Returns new, snapt point vector map"""
    # Snap occurrences to network
    gscript.run_command(
        "v.distance",
        overwrite=True,
        verbose=True,
        from_=snap_objects,
        from_type=snap_type,
        to=river_network,
        to_type="line",
        output=tmp_name,
        dmax=dmax,
        column="to_cat",
        upload="cat",
        table=tmp_name,
    )

    gscript.run_command(
        "v.distance",
        overwrite=True,
        verbose=True,
        from_=tmp_name,
        from_type="line",
        to=snap_objects,
        to_type=snap_type,
        dmax=0.01,
        upload="cat",
        column="from_cat",
    )

    gscript.run_command(
        "v.db.join",
        map=tmp_name,
        column="from_cat",
        other_table=snap_objects,
        other_column="cat",
        subset_columns=column,
    )

    gscript.run_command(
        "v.to.points",
        flags="r",
        overwrite=True,
        verbose=True,
        input=tmp_name,
        type="line,area",
        use="end",
        output="{}_snaped".format(snap_objects),
    )

    return "{}_snaped".format(snap_objects)


def network_analysis(in_g):
    """ """
    measures = ["maximum", "average", "stddev"]
    slope_measures = list(itertools.product(scales, measures))
    for clust in set(in_g.vs["cluster"]):
        print(clust)
        # Get custer graph
        cl_g = in_g.subgraph(set(in_g.vs.select(cluster_eq=clust)))
        print(clust, len(cl_g.es))
        edge_n = len(cl_g.es)
        for idx, cl_edge in enumerate(cl_g.es):
            gscript.percent(idx, edge_n, 1)
            # Get source vertex
            source_vertex = cl_edge.source

            g_edge =  in_g.es.select(cat_eq=cl_edge["cat"])[0]
            g_vertex = in_g.vs.select(name_eq=cl_g.vs[source_vertex]["name"])[0]
            #print(g_edge["cat"])

            # Edges to outlet
            us_g = cl_g.subgraph(sorted([
                            bfs_node.index
                            for bfs_node in cl_g.bfsiter(source_vertex, mode="IN")
                        ] + [source_vertex]))
            ds_g = cl_g.subgraph(sorted([
                            bfs_node.index
                            for bfs_node in cl_g.bfsiter(source_vertex, mode="OUT")
                        ] + [source_vertex]))
            """
            edges_downstreams = g.get_shortest_paths(
                outlet, to=g.vs.select(cluster_eq=outlet["cluster"]), mode="IN", output="epath"
            )
            for epath in edges_downstreams:
                if len(epath) > 0:
                    epath = g.es[epath]
                    vpath = [edge.source for edge in epath]
                    # print(path[-1])
                    end_node = g.vs[vpath[-1]]
            """
            # print(g_vertex)
            downstream_culverts = ds_g.vs.select(culvert=1).select(name_ne=g_vertex["name"])
            downstream_dams = ds_g.vs.select(dam=1).select(name_ne=g_vertex["name"])
            g_edge["downstream_culverts_n"] = len(downstream_culverts)
            g_vertex["downstream_culverts_n"] = g_edge["downstream_culverts_n"]
            g_edge["downstream_dams_n"] = len(downstream_dams)
            g_vertex["downstream_dams_n"] = g_edge["downstream_dams_n"]
            if len(downstream_culverts) > 0:
                g_edge["culverts_downstream"] = ",".join(
                        downstream_culverts["name"]
                    )
                g_vertex["culverts_downstream"] = g_edge["culverts_downstream"]
                for measure in slope_measures:
                    measure_name = f"stream_slope_{measure[0]:02}"
                    g_vertex[f"downstream_culvert_slope_{measure[0]:02}_max"] = max(downstream_culverts[measure_name])
                    g_edge[f"downstream_culvert_slope_{measure[0]:02}_max"] = g_vertex[f"downstream_culvert_slope_{measure[0]:02}_max"]

            if len(downstream_dams) > 0:
                g_edge["dams_downstream"] = ",".join(
                            downstream_dams["name"]
                        )
                g_vertex["dams_downstream"] = g_edge["dams_downstream"]
                for measure in slope_measures:
                    measure_name = f"stream_slope_{measure[0]:02}"
                    g_vertex[f"downstream_culvert_slope_{measure[0]:02}_max"] = max(downstream_dams[measure_name])
                    g_edge[f"downstream_culvert_slope_{measure[0]:02}_max"] = g_vertex[f"downstream_culvert_slope_{measure[0]:02}_max"]

            length_weight = np.array(ds_g.es["length_m"])


            # get max slope in culvert


            for measure in slope_measures:
                measure_name = "dem_1m_carved_slope_{}_{}".format(
                    f"{measure[0]:02}", measure[1]
                )
                measure_value = np.array(ds_g.es[measure_name])

                if measure[1] == "maximum":
                    measure_agg = max(measure_value[~np.isnan(measure_value)])
                else:
                    measure_agg = lwa(measure_value, length_weight)

                g_vertex["downstream_{}".format(measure_name)] = measure_agg
                g_edge["downstream_{}".format(measure_name)] = measure_agg

            upstream_occurrences = us_g.vs.select(occurrence=1)
            g_edge["anadrome_edge"] = int(len(upstream_occurrences) > 0)
            g_vertex["anadrome_edge"] = int(len(upstream_occurrences) > 0)

            upstream_occurrences_1980 = us_g.vs.select(occurrence_1980=1)
            g_edge["anadrome_edge_1980"] = int(len(upstream_occurrences_1980) > 0)
            g_vertex["anadrome_edge_1980"] = int(len(upstream_occurrences_1980) > 0)

            upstream_occurrences_2000 = us_g.vs.select(occurrence_2000=1)
            g_edge["anadrome_edge_2000"] = int(len(upstream_occurrences_2000) > 0)
            g_vertex["anadrome_edge_2000"] = int(len(upstream_occurrences_2000) > 0)

            if len(us_g.es) > 0:
                upstream_culverts = us_g.vs.select(culvert=1).select(name_ne=g_vertex["name"])
                upstream_lakes = us_g.vs.select(lake=1).select(name_ne=g_vertex["name"])
                g_vertex["upstream_length_m"] = sum(
                    us_g.es["length_m"]
                )
                g_vertex["upstream_culverts_n"] = (
                    len(upstream_culverts)
                )
                g_edge["upstream_culverts_n"] = g_vertex["upstream_culverts_n"]
                g_vertex["upstream_dams_n"] = (
                    sum(us_g.vs["dam"]) - g_vertex["dam"]
                )
                g_edge["upstream_dams_n"] = g_vertex["upstream_dams_n"]
                g_vertex["occurrences_upstream_n"] = len(
                    upstream_occurrences
                )
                g_edge["occurrences_upstream_n"] = g_vertex["occurrences_upstream_n"]
                g_vertex["occurrences_upstream"] = ",".join(
                    upstream_occurrences["name"]
                )
                g_vertex["upstream_lakes_n"] = (
                    len(upstream_lakes)
                )
                g_edge["upstream_lakes_n"] = g_vertex["upstream_lakes_n"]
                g_vertex["lake_upstreams"] = (
                    int(len(upstream_lakes) > 0)
                )
                g_edge["lake_upstreams"] = g_vertex["lake_upstreams"]

                us_g.delete_vertices(upstream_culverts)

                if len(us_g.es) > 0:
                    g_vertex["upstream_length_m_uniq"] = sum(
                        us_g.es.select(
                            _within=[
                                bfs_node.index
                                for bfs_node in list(
                                    us_g.bfsiter(
                                        us_g.vs.select(
                                            name_eq=g_vertex["name"]
                                        )[0],
                                        mode="IN",
                                    )
                                )
                            ]
                        )["length_m"]
                    )
                    # if not "upstream_length_m_uniq" in g_vertex.attributes():
                    #     g_vertex["upstream_length_m_uniq"] = sum(g.vs[g.incident(g_vertex, mode="in")]["length_m"])
                else:
                    g_vertex["upstream_length_m_uniq"] = 0

    return in_g


def network_analysis2(in_g):
    """ """
    measures = ["maximum", "average", "stddev"]
    slope_measures = list(itertools.product(scales, measures))
    edge_n = len(in_g.es)
    for idx, g_edge in enumerate(in_g.es):
        gscript.percent(idx, edge_n, 1)
        # Get source vertex
        source_vertex = g_edge.source
        g_vertex = in_g.vs[source_vertex]
        #print(g_edge["cat"])

        # Edges to outlet
        us_g = in_g.subgraph(sorted([
                        bfs_node.index
                        for bfs_node in in_g.bfsiter(source_vertex, mode="IN")
                    ] + [source_vertex]))
        ds_g = in_g.subgraph(sorted([
                        bfs_node.index
                        for bfs_node in in_g.bfsiter(source_vertex, mode="OUT")
                    ] + [source_vertex]))
        """
        edges_downstreams = g.get_shortest_paths(
            outlet, to=g.vs.select(cluster_eq=outlet["cluster"]), mode="IN", output="epath"
        )
        for epath in edges_downstreams:
            if len(epath) > 0:
                epath = g.es[epath]
                vpath = [edge.source for edge in epath]
                # print(path[-1])
                end_node = g.vs[vpath[-1]]
        """
        # print(g_vertex)
        downstream_culverts = ds_g.vs.select(culvert=1).select(name_ne=g_vertex["name"])
        downstream_dams = ds_g.vs.select(dam=1).select(name_ne=g_vertex["name"])
        g_edge["downstream_culverts_n"] = len(downstream_culverts)
        g_vertex["downstream_culverts_n"] = g_edge["downstream_culverts_n"]
        g_edge["downstream_dams_n"] = len(downstream_dams)
        g_vertex["downstream_dams_n"] = g_edge["downstream_dams_n"]
        if len(downstream_culverts) > 0:
            g_edge["culverts_downstream"] = ",".join(
                    downstream_culverts["name"]
                )
            g_vertex["culverts_downstream"] = g_edge["culverts_downstream"]
            for measure in slope_measures:
                measure_name = f"stream_slope_{measure[0]:02}"
                g_vertex[f"downstream_culvert_slope_{measure[0]:02}_max"] = max(downstream_culverts[measure_name])
                g_edge[f"downstream_culvert_slope_{measure[0]:02}_max"] = g_vertex[f"downstream_culvert_slope_{measure[0]:02}_max"]

        if len(downstream_dams) > 0:
            g_edge["dams_downstream"] = ",".join(
                        downstream_dams["name"]
                    )
            g_vertex["dams_downstream"] = g_edge["dams_downstream"]
            for measure in slope_measures:
                measure_name = f"stream_slope_{measure[0]:02}"
                g_vertex[f"downstream_culvert_slope_{measure[0]:02}_max"] = max(downstream_dams[measure_name])
                g_edge[f"downstream_culvert_slope_{measure[0]:02}_max"] = g_vertex[f"downstream_culvert_slope_{measure[0]:02}_max"]

        length_weight = np.array(ds_g.es["length_m"])


        # get max slope in culvert


        for measure in slope_measures:
            measure_name = "dem_1m_carved_slope_{}_{}".format(
                f"{measure[0]:02}", measure[1]
            )
            measure_value = np.array(ds_g.es[measure_name])

            if measure[1] == "maximum":
                measure_agg = max(measure_value[~np.isnan(measure_value)])
            else:
                measure_agg = lwa(measure_value, length_weight)

            g_vertex["downstream_{}".format(measure_name)] = measure_agg
            g_edge["downstream_{}".format(measure_name)] = measure_agg

        upstream_occurrences = us_g.vs.select(occurrence=1)
        g_edge["anadrome_edge"] = int(len(upstream_occurrences) > 0)

        if len(us_g.es) > 0:
            upstream_culverts = us_g.vs.select(culvert=1).select(name_ne=g_vertex["name"])
            upstream_lakes = us_g.vs.select(lake=1).select(name_ne=g_vertex["name"])
            g_vertex["upstream_length_m"] = sum(
                us_g.es["length_m"]
            )
            g_vertex["upstream_culverts_n"] = (
                len(upstream_culverts)
            )
            g_edge["upstream_culverts_n"] = g_vertex["upstream_culverts_n"]
            g_vertex["upstream_dams_n"] = (
                sum(us_g.vs["dam"]) - g_vertex["dam"]
            )
            g_edge["upstream_dams_n"] = g_vertex["upstream_dams_n"]
            g_vertex["occurrences_upstream_n"] = len(
                upstream_occurrences
            )
            g_edge["occurrences_upstream_n"] = g_vertex["occurrences_upstream_n"]
            g_vertex["occurrences_upstream"] = ",".join(
                upstream_occurrences["name"]
            )
            g_vertex["upstream_lakes_n"] = (
                len(upstream_lakes)
            )
            g_edge["upstream_lakes_n"] = g_vertex["upstream_lakes_n"]
            g_vertex["lake_upstreams"] = (
                int(len(upstream_lakes) > 0)
            )
            g_edge["lake_upstreams"] = g_vertex["lake_upstreams"]

            us_g.delete_vertices(upstream_culverts)

            if len(us_g.es) > 0:
                g_vertex["upstream_length_m_uniq"] = sum(
                    us_g.es.select(
                        _within=[
                            bfs_node.index
                            for bfs_node in list(
                                us_g.bfsiter(
                                    us_g.vs.select(
                                        name_eq=g_vertex["name"]
                                    )[0],
                                    mode="IN",
                                )
                            )
                        ]
                    )["length_m"]
                )
                # if not "upstream_length_m_uniq" in g_vertex.attributes():
                #     g_vertex["upstream_length_m_uniq"] = sum(g.vs[g.incident(g_vertex, mode="in")]["length_m"])
            else:
                g_vertex["upstream_length_m_uniq"] = 0

    return in_g

with Session(gisdb=grassdb, location=location, mapset=mapset, create_opts=""):
    import grass.script as gscript
    from grass.pygrass.vector.table import get_path # *

    gscript.run_command(
        "g.mapsets",
        overwrite=True,
        verbose=True,
        operation="add",
        mapset=source_mapsets,
    )

    #gscript.run_command(
    #    "g.remove", type="vector", name="{0}".format(streams), flags="f"
    #)

    gscript.run_command(
        "v.extract",
        overwrite=True,
        verbose=True,
        input=occurrences,
        type="point",
        output="{}_presence".format(occurrences),
        where="lower(g_occurrenceStatus) = 'present' OR g_occurrenceStatus IS NULL"
    )
    occurrences = "{}_presence".format(occurrences)

    for vmap in NVE_barriers:
        gscript.run_command("g.copy", vector="{0},{0}".format(vmap))


    gscript.run_command(
        "g.region", overwrite=True, verbose=True, raster=elevation, flags="p"
    )
    # gscript.run_command('g.copy', vector='{0}@p_eel_FKB_N502,{0}'.format(occurrences))
    # gscript.run_command('g.copy', vector='{0}@p_eel_FKB_N502,{0}'.format(full_network))

    # Snap occurrences and potential barriers to river network
    snaped_objects = []
    snaped_objects.append(snap_join(river_network=streams,
                  snap_objects=occurrences,
                  tmp_name=lines,
                  snap_type="point",
                  dmax=150,
                  column="g_occurrenceid"))
    for idx, nve_map in enumerate(NVE_barriers):
        snaped_objects.append(snap_join(river_network=streams,
                      snap_objects=nve_map,
                      tmp_name=NVE_snaplines[idx],
                      snap_type="point,line",
                      dmax=25,
                      column="objType"))

    # Split river network in lake boundaries
    gscript.run_command(
        "v.type",
        overwrite=True,
        verbose=True,
        input=lakes,
        output="{}_line".format(lakes.split("@")[0]),
        from_type="boundary",
        to_type="line",
    )
    gscript.run_command(
        "v.extract",
        overwrite=True,
        verbose=True,
        input="{}_line".format(lakes.split("@")[0]),
        layer="-1",
        type="line",
        output="{}_line_only".format(lakes.split("@")[0]),
    )

    # Breaking river network lines in lake boundaries
    gscript.run_command(
        "v.patch",
        flags="b",
        overwrite=True,
        verbose=True,
        input="{},{},{}_line_only".format(
            streams, ",".join(barriers), lakes.split("@")[0]
        ),
        output="tmp",
    )
    barriers.append(barriers_pol)

    # Breaking lines fails if there are lines with 0 length in the input
    gscript.run_command(
        "v.clean",
        flags="c",
        overwrite=True,
        verbose=True,
        input="tmp",
        type="line",
        output="tmp2",
        layer="1",
        error="intersections",
        tool="rmline,break",
    )
    gscript.run_command(
        "v.category",
        input="intersections",
        type="point",
        option="add",
        output="intersections_cat",
        overwrite=True,
    )

    gscript.run_command(
        "v.extract",
        overwrite=True,
        verbose=True,
        input="intersections_cat",
        output="intersection_points",
        type="point",
    )

    # For barriers digitized as polygons
    gscript.run_command(
        "v.overlay",
        overwrite=True,
        verbose=True,
        flags="t",
        ainput=streams,
        alayer="1",
        atype="line",
        binput="Norway_FKB_Veg_polygons",
        blayer="1",
        btype="area",
        output="intersection_lines",
        operator="and",
    )

    # v.patch --overwrite --verbose input=intersection_points,intersection_lines output=intersection_all
    # v.distance --overwrite --verbose from=intersection_all from_type="point,line" \
    # to=intersection_all to_type="point,line" output=intersection_points_connections dmax=15 dmin=0.0001
    # v.net.components -a --overwrite input=intersection_lines_center_connections@p_agder_eel_net \
    # output=intersection_lines_center_connections_strong method=strong

    gscript.run_command(
        "v.centerpoint",
        overwrite=True,
        verbose=True,
        input="intersection_lines",
        output="intersection_lines_center",
        type="line",
    )

    # Merge occurrences and culverts and connect them with network
    gscript.run_command(
        "v.patch",
        # flags="b",
        overwrite=True,
        verbose=True,
        input="{},intersection_points,intersection_lines_center".format(
            ",".join(snaped_objects)
        ),
        output="nodes_new",
    )

    gscript.run_command(
        "v.split", flags="f",
        overwrite=True,
        verbose=True,
        input=streams,
        output="{}_split".format(streams),
        length=1000,
        units="meters",
        )
    streams = "{}_split".format(streams)

    gscript.run_command(
        "v.net",
        overwrite=True,
        flags="cs",
        input=streams,
        points="nodes_new",
        output="TEST",
        operation="connect",
        arc_layer="1",
        arc_type="line",
        node_layer="2",
        threshold=0.01,
        turn_layer="3",
        turn_cat_layer="4",
    )

    gscript.run_command(
        "v.category",
        overwrite=True,
        verbose=True,
        input="TEST",
        layer="3",
        type="line",
        output="TEST2",
        option="add",
        cat=1,
        step=1,
    )
    gscript.run_command(
        "v.extract",
        overwrite=True,
        input="TEST2",
        type="line",
        layer="3",
        output="TEST3",
    )

    gscript.run_command(
        "v.clean",
        flags="c",
        overwrite=True,
        verbose=True,
        input="TEST3",
        type="line",
        output="TEST4",
        layer="3",
        tool="rmdupl,rmline,break",
    )

    # gscript.run_command('v.category', overwrite=True, verbose=True,
    #                     input="TEST3@p_eel_FKB_N502",layer="2",
    #                     type="point", output="TEST4@p_eel_FKB_N502",
    #                     option="add", cat=1, step=1)
    #
    # gscript.run_command('v.extract', overwrite=True, verbose=True,
    #                     input=N50_roads_org, output='roads_l',
    #                     where="MEDIUM = 'L'")
    # gscript.run_command('v.extract', overwrite=True, verbose=True,
    #                     input=N50_bane_org, output='bane_l',
    #                     where="MEDIUM = 'L'")
    #
    # gscript.run_command('v.db.join',  map=streams, layer='2', column='cat',
    #                     other_table=lines, other_column='to_cat',
    #                     subset_columns='g_occurrenceid')
    #

    gscript.run_command(
        "v.net",
        flags="c",
        input=input,
        arc_layer=arc_layer,
        output=full_network,
        operation="nodes",
        node_layer=node_layer,
        overwrite=True,
        verbose=True,
    )

    gscript.run_command(
        "v.db.addtable",
        verbose=True,
        map=full_network,
        layer=arc_layer,
        table="{}_3".format(full_network),
    )

    # # Compute slope for network segments
    # raster_maps = ','.join(['slope_{}'.format(scale) for scale in scales])
    # gscript.run_command('g.region', raster='slope_{}'.format(scales[0]), flags='p')
    #
    # gscript.run_command('r.slope.direction',
    #                  elevation=elevation,
    #                  direction='DTM_1m_flow_dir_cleaned@p_eel_FKB_N502',
    #                  dir_type='auto', steps='1,11,21,31', slope_measure='degree',
    #                  output='slope_1,slope_11,slope_21,slope_31')
    #
    # with Session(gisdb=grassdb, location=location, mapset=mapset, create_opts=""):

    raster_maps = "dem_1m_carved_slope_03@p_Agder_aal,dem_1m_carved_slope_09@p_Agder_aal,\
dem_1m_carved_slope_21@p_Agder_aal,dem_1m_carved_slope_31@p_Agder_aal,dem_1m_carved_slope_51@p_Agder_aal,\
dem_1m_carved_SPI_filled@p_Agder_aal"
    # ','.join(['slope_{}'.format(scale) for scale in scales])
    gscript.run_command(
        "v.rast.stats",
        map=full_network,  # type="line",
        layer=arc_layer,
        raster=raster_maps,
        column_prefix=raster_maps.replace("@p_Agder_aal", ""),
        flags="c",
        method="maximum,average,stddev",
    )

    # Run again with categories that were not rasterized (e.g. because of overlap)
    gscript.run_command(
        "v.rast.stats",
        map=full_network,  # type="line",
        layer=arc_layer,
        raster=raster_maps,
        column_prefix=raster_maps.replace("@p_Agder_aal", ""),
        flags="c",
        method="maximum,average,stddev",
        where="dem_1m_carved_slope_03_maximum IS NULL",
    )


    gscript.run_command(
        "v.to.db",
        map=full_network,
        layer=arc_layer,
        type="line",
        option="length",
        column="length_m",
        overwrite=True,
        quiet=True,
    )

    # Data has to be parsed or written to file as StringIO objects are not supported by igraph
    # https://github.com/igraph/python-igraph/issues/8
    net = (
        gscript.read_command(
            "v.net",
            input=full_network,
            points=full_network,
            node_layer=node_layer,
            arc_layer=arc_layer,
            operation="report",
            quiet=True,
        )
        .rstrip("\n")
        .split("\n")
    )


    edge_attrs = np.genfromtxt(
        #BytesIO(
            gscript.read_command(
                "v.db.select", map=full_network, layer="3", separator=",", quiet=True
            )
            .rstrip("\n").split("\n"),
            #.encode()
        #),
        dtype=None,
        names=True,
        delimiter=",",
    )

    # Parse network data and extract vertices, edges and edge names
    edges = []
    vertices = []
    edge_cat = []
    for l in net:
        if l != "":
            # Names for edges and vertices have to be of type string
            # Names (cat) for edges
            edge_cat.append(l.split(" ")[0])

            # From- and to-vertices for edges
            edges.append((l.split(" ")[1], l.split(" ")[2]))

            # Names (cat) for from-vertices
            vertices.append(l.split(" ")[1])

            # Names (cat) for to-vertices
            vertices.append(l.split(" ")[2])

    # Create Graph object
    g = Graph().as_directed()

    # Add vertices with names
    vertices.sort()
    vertices = set(vertices)
    g.add_vertices(list(vertices))

    # Add edges with names
    g.add_edges(edges)
    g.es["cat"] = edge_cat

    # Add edge attributes
    for colname in edge_attrs.dtype.names[1:]:
        g.es[colname] = edge_attrs[colname]

    # Mark occurrence vertices
    occs = np.genfromtxt(gscript.read_command(
        "v.distance",
        overwrite=True,
        verbose=True,
        from_=lines,
        from_type="line",
        to_layer=node_layer,
        to=full_network,
        to_type="point",
        flags="p",
        dmax=0.01,
        upload="cat",
    ).rstrip().split("\n"), delimiter="|", names=True, dtype=None)

    # occ_cats = [occ.split("|")[1] for occ in occs]

    g.vs["occurrence"] = 0
    g.vs.select(name_in=occs['cat'].astype(str))["occurrence"] = 1

    occ_1980 = np.genfromtxt(gscript.read_command("v.db.select", map=occurrences, columns="cat", where="g_year > 1980").rstrip().split("\n"), delimiter="|", names=True, dtype=None)
    occ_1980_cats = sorted(occs["cat"][np.isin(occs["from_cat"], occ_1980["cat"])].astype(str).tolist())
    g.vs["occurrence_1980"] = 0
    g.vs.select(name_in=occ_1980_cats)["occurrence_1980"] = 1
    occ_2000 = np.genfromtxt(gscript.read_command("v.db.select", map=occurrences, columns="cat", where="g_year > 2000").rstrip().split("\n"), delimiter="|", names=True, dtype=None)
    occ_2000_cats = sorted(occs["cat"][np.isin(occs["from_cat"], occ_2000["cat"])].astype(str).tolist())
    g.vs["occurrence_2000"] = 0
    g.vs.select(name_in=occ_2000_cats)["occurrence_2000"] = 1

    # Mark potential culvert vertices
    culverts = []
    for bar in barriers:
        culverts.extend(gscript.read_command(
            "v.distance",
            overwrite=True,
            verbose=True,
            from_=full_network, # Here we need to use barriers only (not all new nodes)
            from_type="point",
            from_layer=node_layer,
            to=bar,
            to_type="point,line,area",
            flags="p",
            dmax=1.0,
            upload="cat,dist",
        ).split("\n")[1:-1])

    culvert_cats = {culvs.split("|")[0] for culvs in culverts if not "null" in culvs}

    g.vs["culvert"] = 0
    g.vs.select(name_in=culvert_cats)["culvert"] = 1

    # Mark lake vertices
    lake_points = gscript.read_command(
        "v.distance",
        overwrite=True,
        verbose=True,
        from_=lakes,
        from_type="area",
        to_layer=node_layer,
        to=full_network,
        to_type="point",
        flags="p",
        dmax=0.1,
        upload="cat",
    ).split("\n")[1:-1]

    lake_cats = [lp.split("|")[1] for lp in lake_points if not "null" in lp]

    g.vs["lake"] = 0
    g.vs.select(name_in=set(lake_cats))["lake"] = 1

    # measure slope at nodes (e.g. within culvert/bridge)
    rmaps = ",".join([f"dem_1m_carved_slope_{scale:02}@p_Agder_aal" for scale in scales])
    slope_at_nodes = np.genfromtxt(gscript.read_command("r.what", flags="cv", map=rmaps, points=full_network).strip().split("\n"), delimiter="|", dtype="i4")
    for scale in scales:
        g.vs[f"stream_slope_{scale:02}"] = 0
    for node in slope_at_nodes:
        vtx = g.vs.find(name_eq=str(node[0]))
        for idx, scale in enumerate(scales):
            vtx[f"stream_slope_{scale:02}"] = node[4 + idx]

    height_at_nodes = np.genfromtxt(gscript.read_command("r.what", flags="v", map="dem_1m_carved", points=full_network).strip().split("\n"), delimiter="|", dtype=None, missing_values="*")
    for node in height_at_nodes:
        vtx = g.vs.find(name_eq=str(node[0]))
        vtx["hoh"] = node[4]

    # Outlet distance to sea
    # Mark outlets to ocean
    hav_dist = gscript.read_command(
        "v.distance",
        overwrite=True,
        verbose=True,
        from_=full_network,
        from_type="point",
        from_layer=node_layer,
        to="Norway_FKB_Hav",
        to_type="area",
        dmax=10.0,
        upload="dist",
        flags="p",
    ).strip().split("\n")
    hav_dist_at_nodes = np.genfromtxt(hav_dist, delimiter="|", dtype=None, names=True, missing_values="null")
    for node in hav_dist_at_nodes[np.where(~np.isnan(hav_dist_at_nodes["dist"]))]:
        vtx = g.vs.find(name_eq=str(node[0]))
        vtx["avstand_hav"] = node[1]

    # Mark dam vertices
    g.vs["dam"] = 0
    dam_cats = []
    for idx, nve_map in enumerate(NVE_snaplines):
        snaped_objects.append(snap_join(river_network=streams,
                      snap_objects=nve_map,
                      tmp_name=NVE_snaplines[idx],
                      snap_type="point,line",
                      dmax=25,
                      column="objType"))

        dams = gscript.read_command(
            "v.distance",
            overwrite=True,
            verbose=True,
            from_=nve_map,
            from_type="point,line",
            to_layer=node_layer,
            to=full_network,
            to_type="point",
            flags="p",
            dmax=1.0,
            upload="cat",
        ).split("\n")[1:-1]

    dam_cats += [dam.split("|")[1] for dam in dams if not "null" in dam]

    g.vs.select(name_in=set(dam_cats))["dam"] = 1
    # reclassify road crossings at dams
    g.vs.select(dam_eq=1)["culvert"] = 0

    # Mark bridge and tunel vertices
    bridges = []
    tunnels = []
    for vmap in barriers:
        medium_col = [
            s.split("|")[1]
            for s in gscript.read_command("v.info", map=vmap, flags="c")
            .strip()
            .split("\n")
            if "medium" in s.lower()
        ][0]

        for m in ["L", "U"]:  # U = tunnel, L = bridge
            gscript.run_command(
                "v.extract",
                overwrite=True,
                verbose=True,
                input=vmap,
                output="barrier_medium_l",
                where="{0} = '{1}' OR {0} = '{2}'".format(medium_col, m, m.lower()),
            )

            m_cats = gscript.read_command(
                "v.distance",
                overwrite=True,
                verbose=True,
                from_="barrier_medium_l",
                from_type="area,line,point",
                to_layer=node_layer,
                to=full_network,
                to_type="point",
                flags="pa",
                dmax=5,
                upload="cat",
            ).split("\n")[1:-1]
            if m == "L":
                bridges.extend(m_cats)
            else:
                tunnels.extend(m_cats)

    bridges += gscript.read_command(
        "v.distance",
        overwrite=True,
        verbose=True,
        from_="NVDB_60@p_eel_FKB_N50",
        from_type="area,line,point",
        to_layer=node_layer,
        to=full_network,
        to_type="point",
        flags="pa",
        dmax=5,
        upload="cat",
    ).split("\n")[1:-1]

    bridge_cats = {bridge.split("|")[1] for bridge in bridges if not "null" in bridge}
    tunnel_cats = {tunnel.split("|")[1] for tunnel in tunnels if not "null" in tunnel}

    g.vs["bridge"] = 0
    g.vs.select(name_in=bridge_cats)["bridge"] = 1
    g.vs.select(bridge=1)["culvert"] = 0

    g.vs["tunnel"] = 0
    g.vs.select(name_in=tunnel_cats)["tunnel"] = 1
    g.vs.select(tunnel=1)["culvert"] = 0

    # Identify clusters
    clusters = g.as_undirected().clusters()
    # max(clusters.sizes())
    g.vs["cluster"] = g.as_undirected().clusters().membership

    for cl in set(g.vs["cluster"]):
        print("Cluster: ", cl)
        cl_vtx = g.vs.select(cluster_eq=cl)
        cluster_to_sea = 1 if min(i for i in cl_vtx["avstand_hav"] + [9999] if i is not None) <= 10 or min(i for i in cl_vtx["hoh"] + [9999] if i is not None) <= 5 else 0
        cl_vtx["cluster_to_sea"] = cluster_to_sea
        for vtx in cl_vtx:
            g.es[g.incident(vtx, mode="all")]["cluster"] = cl
            g.es[g.incident(vtx, mode="all")]["cluster_to_sea"] = cluster_to_sea

    # if limit_to_occ:
    #     # Remove clusters without occurrences
    #     clusters_rel = set(g.vs.select(occurrence_eq=1)['cl'])
    #
    #     g.delete_vertices(g.vs.select(cl_notin=clusters_rel))

    # Compute number of vertices that can be reached from each vertex
    # Indicates upstream or downstream position of a node
    g.vs["neighborhood"] = g.neighborhood_size(mode="out", order=g.diameter())

    # Compute incoming degree centrality
    # sources have incoming degree centrality of 0
    g.vs["indegree"] = g.degree(mode="in")

    # Compute outgoing degree centrality
    # outlets have outgoing degree centrality of 0
    g.vs["outdegree"] = g.degree(mode="out")

    g.vs["uddegree"] = g.degree(mode="all")

    g.vs["node_type"] = None  # 'other'
    g.vs.select(indegree=0)["node_type"] = "source"
    g.vs.select(outdegree=0)["node_type"] = "outlet"
    g.vs.select(indegree_gt=1, outdegree_gt=0)["node_type"] = "confluence"
    g.vs.select(indegree=1, outdegree=1)["node_type"] = "other"

    # # Mark all streams downstreams of lakes (could be selected lakes based on altitude or slope in stream)
    lake_upstreams = set(
    itertools.chain(
         *g.neighborhood(g.vs.select(lake_eq=1), mode="OUT", order=g.diameter())
    )
    )
    g.es["lake_upstreams"] = 0
    g.es.select(_within=lake_upstreams)["lake_upstreams"] = 1
    g.vs["lake_upstreams"] = 0
    g.vs[lake_upstreams]["lake_upstreams"] = 1

    # Downstream measures
    g.es["anadrome_edge"] = 0
    g.vs["upstream_length_m"] = 0
    g.vs["upstream_culverts_n"] = 0
    g.vs["upstream_dams_n"] = 0
    g.vs["upstream_length_m_not_uniq"] = 0
    g.vs["upstream_length_m_uniq"] = 0

    measures = ["maximum", "average", "stddev"]
    slope_measures = list(itertools.product(scales, measures))

    g.es["anadrome_edge_1980"] = 0
    a_edges_1980 = [[ep for ep in g.get_shortest_paths(outlet, g.vs.select(occurrence_1980=1), mode="IN", output="epath") if ep] for outlet in g.vs.select(node_type="outlet")]
    g.es[{i for i in itertools.chain(*[e for e in itertools.chain(*a_edges_1980)])}]["anadrome_edge_1980"] = 1
    g.es["anadrome_edge_2000"] = 0
    a_edges_2000 = [[ep for ep in g.get_shortest_paths(outlet, g.vs.select(occurrence_2000=1), mode="IN", output="epath") if ep] for outlet in g.vs.select(node_type="outlet")]
    g.es[{i for i in itertools.chain(*[e for e in itertools.chain(*a_edges_2000)])}]["anadrome_edge_2000"] = 1

    out_g = network_analysis2(g)

    gscript.verbose(_("Writing result to table..."))

    # Connect table to output edge layer
    export_node_layer = str(int(node_layer) + 2)
    export_edge_layer = '5'

    gscript.run_command("v.category", overwrite=True, verbose=True, flags="t", input=full_network, layer=f"{node_layer},{export_node_layer}", output="{}_cat".format(full_network), option="transfer")
    gscript.run_command("v.category", overwrite=True, verbose=True, flags="t", input="{}_cat".format(full_network), layer=f"{arc_layer},{export_edge_layer}", output="{}_cat2".format(full_network), option="transfer")


    # Write results back to attribute table
    # Note: Backend depenent! For a more general solution this has to be handled
    db_path = "$GISDBASE/$LOCATION_NAME/$MAPSET/sqlite/sqlite.db"
    conn = sqlite3.connect(get_path(db_path))
    c = conn.cursor()
    c.execute("DROP TABLE IF EXISTS {}".format(table))

    create_str = "CREATE TABLE {}(".format(table)

    cols = []
    col_types = {}
    for attr in out_g.vs.attributes():
        if attr == "name":
            col_name = "cat"
            col_type = "integer"
            map_function = int
        else:
            col_name = attr
            ltypes = {item_check(attr_val) for attr_val in out_g.vs[attr]}
            if "other" in ltypes:
                col_type = "text"
                map_function = str
            elif "float" in ltypes:
                col_type = "real"
                map_function = float
            else:
                col_type = "integer"
                map_function = int

        col_types[attr] = map_function
        cols.append("{} {}".format(col_name, col_type))

    create_str += ",".join(cols)
    create_str += ")"
    create_str += ";"

    # Create temporary table
    c.execute(create_str)
    conn.commit()

    # Get Attributes
    attrs = []
    for n in out_g.vs:
        attr_list = [
            col_types[attr](n[attr]) if n[attr] else None for attr in out_g.vs.attributes()
        ]
        attrs.append(tuple(attr_list))

    # Insert data into temporary table
    c.executemany(
        "INSERT INTO {} VALUES ({})".format(
            table, ",".join(["?"] * len(out_g.vs.attributes()))
        ),
        attrs,
    )

    # Save (commit) the changes
    conn.commit()

    # We can also close the connection if we are done with it.
    # Just be sure any changes have been committed or they will be lost.
    conn.close()


    gscript.run_command(
        "v.db.connect",
        map="{}_cat2".format(full_network),
        table=table,
        layer=export_node_layer,
        flags="o",
    )
    gscript.run_command("db.execute", sql="CREATE INDEX {table}_{col}_idx ON {table}({col});".format(col="cat", table=table))
    gscript.run_command("db.select", sql="SELECT * FROM {table} LIMIT 5;".format(table=table))

    # Write edge attributes
    conn = sqlite3.connect(get_path(db_path))
    c = conn.cursor()
    edge_table = "{}".format(table.replace("_2", ""))
    c.execute("DROP TABLE IF EXISTS {}".format(edge_table))
    conn.commit()

    create_str = "CREATE TABLE {}(".format(edge_table)

    cols = []
    col_types = {}
    for attr in out_g.es.attributes():
        if attr == "name" or attr == "cat":
            col_name = "cat"
            col_type = "integer"
            map_function = int
        else:
            col_name = attr
            ltypes = {item_check(attr_val) for attr_val in out_g.es[attr]}
            if "other" in ltypes:
                col_type = "text"
                map_function = str
            elif "float" in ltypes:
                col_type = "real"
                map_function = float
            else:
                col_type = "integer"
                map_function = int

        col_types[attr] = map_function
        cols.append("{} {}".format(col_name, col_type))

    create_str += ",".join(cols)
    create_str += ");"

    # Save (commit) the changes
    c.execute(create_str)
    conn.commit()

    # Get Attributes
    attrs = []
    for n in out_g.es:
        attr_list = [
            col_types[attr](n[attr]) if n[attr] else None
            for attr in out_g.es.attributes()
        ]
        attrs.append(tuple(attr_list))

    # Insert data into temporary table
    attrs_n = len(out_g.es.attributes())
    c.executemany(
        "INSERT INTO {} VALUES ({})".format(
            edge_table, ",".join(["?"] * attrs_n)
        ),
        attrs,
    )

    # Save (commit) the changes
    conn.commit()

    # We can also close the connection if we are done with it.
    # Just be sure any changes have been committed or they will be lost.
    conn.close()

    #
    # Connect table to output node layer
    gscript.run_command(
        "v.db.connect",
        map="{}_cat2".format(full_network),
        table=edge_table,
        layer=export_edge_layer,
        flags="o",
    )
    gscript.run_command("db.execute", sql="CREATE INDEX {table}_{col}_idx ON {table}({col});".format(col="cat", table=edge_table))
    gscript.run_command("db.select", sql="SELECT * FROM {table} LIMIT 5;".format(table=edge_table))

    # Join temporary table to output
    # gscript.run_command('v.db.join', map=full_network, layer=node_layer,
    #                    column='cat', other_table=tmpTable,
    #                    other_column='cat', quiet=True)

    # gscript.run_command('v.db.addtable', map=full_network, layer=2, key='cat')
    # gscript.run_command('v.to.db', map=full_network, layer=node_layer, option='cat', columns='cat', quiet=True)
    gscript.run_command("g.copy", vector="Norway_N50_VegSti,N50_roads")
    gscript.run_command(
        "v.db.addcolumn",
        overwrite=True,
        verbose=True,
        map="{}_cat2".format(full_network),
        layer=export_node_layer,
        column="N50_cat integer",
    )
    gscript.run_command(
        "v.distance",
        overwrite=True,
        verbose=True,
        from_="{}_cat2".format(full_network),
        from_type="point",
        from_layer=export_node_layer,
        to="N50_roads",
        to_type="line",
        dmax=0.01,
        upload="cat",
        column="N50_cat",
    )
    columns = "MEDIUM,OBJTYPE,VEGKATEGORI"
    gscript.run_command(
        "v.db.join",
        map="{}_cat2".format(full_network),
        layer=export_node_layer,
        column="N50_cat",
        other_table="N50_roads",
        other_column="cat",
        subset_columns=columns,
    )
    gscript.run_command(
        "v.db.renamecolumn",
        map="{}_cat2".format(full_network),
        layer=export_node_layer,
        column="MEDIUM,medium2",
    )
    gscript.run_command(
        "v.db.renamecolumn",
        map="{}_cat2".format(full_network),
        layer=export_node_layer,
        column="medium2,medium",
    )

    gscript.run_command("g.copy", vector="Norway_FKB_Bane_lines,FKB_Bane")
    gscript.run_command(
        "v.db.addcolumn",
        overwrite=True,
        verbose=True,
        map="{}_cat2".format(full_network),
        layer=export_node_layer,
        column="FKB_cat integer",
    )
    gscript.run_command(
        "v.distance",
        overwrite=True,
        verbose=True,
        from_="{}_cat2".format(full_network),
        from_type="point",
        from_layer=export_node_layer,
        to="FKB_Bane",
        to_type="line",
        dmax=0.01,
        upload="cat",
        column="FKB_cat",
    )
    columns = "medium,objekttypenavn,jernbanetype"
    gscript.run_command(
        "v.db.join",
        map="{}_cat2".format(full_network),
        layer=export_node_layer,
        column="FKB_cat",
        other_table="FKB_Bane",
        other_column="cat",
        subset_columns=columns,
    )






    nvdb_columns = {60:
    79:
    }




    for nvdbid in nvdb_columns:
        # gscript.run_command('g.copy', vector='NVDB_object_{0}@p_CulvertFragmentation_stefan.blumentrath,NVDB_object_{0}'.format(nvdbid))
        map = "NVDB_{}".format(nvdbid)
        # Need to copy for v.db.join
        gscript.run_command("g.copy", vector="{0},{0}".format(map))
        gscript.run_command(
            "v.db.addcolumn",
            overwrite=True,
            verbose=True,
            map="{}_cat2".format(full_network),
            layer=export_node_layer,
            column="NVDB_{}_cat integer".format(nvdbid),
        )
        gscript.run_command(
            "v.distance",
            overwrite=True,
            verbose=True,
            from_="{}_cat2".format(full_network),
            from_type="point",
            from_layer=export_node_layer,
            to=map,
            to_type="line,point",
            dmax=10,
            upload="cat",
            column="NVDB_{}_cat".format(nvdbid),
        )
        gscript.run_command(
            "v.db.join",
            map="{}_cat2".format(full_network),
            layer=export_node_layer,
            column="NVDB_{}_cat".format(nvdbid),
            other_table=map,
            other_column="cat",
            subset_columns=nvdb_columns[nvdbid],
        )

    gscript.run_command(
        "v.db.addcolumn",
        overwrite=True,
        verbose=True,
        map="{}_cat2".format(full_network),
        column="hav_dist real",
        layer=export_node_layer,
    )
    # Mark outlets to ocean
    gscript.run_command(
        "v.distance",
        overwrite=True,
        verbose=True,
        from_="{}_cat2".format(full_network),
        from_type="point",
        from_layer=export_node_layer,
        to="Norway_FKB_Hav",
        to_type="area",
        dmax=5.0,
        upload="dist",
        column="hav_dist",
    )

    gscript.run_command(
        "v.extract",
        overwrite=True,
        verbose=True,
        input="{}_cat2".format(full_network),
        layer=export_node_layer,
        type="point",
        where="node_type = 'outlet' AND hav_dist IS NOT NULL",
        output="outlets",
    )


cols = gscript.read_command("v.info", flags="c", map="{}_cat2".format(full_network), layer="5").strip().split("\n")
for col in cols:
    if col.startswith("INT"):
        print(col.split("|")[1])
        gscript.run_command("v.db.update", map="{}_cat2".format(full_network), layer="5", column=col.split("|")[1], value=0, where="{col} IS NULL".format(col=col.split("|")[1]))

#[col.split("|")[1] for col in cols if col.startswith("INT")]

gscript.read_command("v.to.rast.multi", flags="d", overwrite=True, verbose=True, input="{}_cat2".format(full_network), layer=5, type="line", where="cluster_to_sea > 0", key_column="cat", output="dem_1m_carved_streams_cleaned_full_network", attribute_columns=",".join([col.split("|")[1] for col in cols if col.startswith("INT")][6:]), memory=30000)

!printf "1 = 1\n* = NULL" | r.reclass --o --v input=dem_1m_carved_streams_cleaned_full_network_anadrome_edge output=aal rules="-"
!printf "1 = 1\n* = NULL" | r.reclass --o --v input=dem_1m_carved_streams_cleaned_full_network_anadrome_edge_1980 output=aal_etter_1980 rules="-"
!printf "1 = 1\n* = NULL" | r.reclass --o --v input=dem_1m_carved_streams_cleaned_full_network_anadrome_edge_2000 output=aal_etter_2000 rules="-"

!printf "* = 1\n" | r.reclass --o --v input=dem_1m_carved_streams_cleaned_full_network_cat output=bgr rules="-"

#!r.reclass --o --v input=dem_1m_carved_streams_cleaned_full_network_occurrences_upstream_n output=aal_habitat rules=/home/shared/grassdata/ETRS_33N/p_Agder_aal_net/.tmp/vm-srv-wallace/9897.0

!r.random -b --overwrite --verbose input=dem_1m_carved_streams@p_Agder_aal cover=bgr npoints=2000000 raster=random_points_2000000 vector=random_points_2000000
#!r.random -b --overwrite --verbose input=dem_1m_carved_streams@p_Agder_aal cover=dem_1m_carved_streams_cleaned_full_network_anadrome_edge npoints=2000000 raster=random_points_2000000 vector=random_points_2000000

!r.mapcalc --overwrite --verbose expression="aal_random=if(random_points_2000000 & aal, 1, null())"
!r.mapcalc --overwrite --verbose expression="aal_etter_1980_random=if(random_points_2000000 & aal_etter_1980, 1, null())"
!r.mapcalc --overwrite --verbose expression="aal_etter_2000_random=if(random_points_2000000 & aal_etter_2000, 1, null())"
!r.mapcalc --overwrite --verbose expression="bgr_random=if(random_points_2000000 & bgr, 1, null())"

!r.mapcalc --overwrite --verbose expression="dem_1m_carved_flow_accum_abs=if(dem_1m_carved_flow_accum@p_Agder_aal<0, -1*dem_1m_carved_flow_accum@p_Agder_aal, dem_1m_carved_flow_accum@p_Agder_aal)"

gscript.read_command("r.out.maxent_swd", flags="z", overwrite=True, verbose=True,
alias_input="/opt/src/agder_eel/aal_alias.txt", bgr_output="/opt/src/agder_eel/bgr.swd",
bgr_mask="bgr_random", species_masks="aal_random,aal_etter_1980_random,aal_etter_2000_random",
species_output="/opt/src/agder_eel/aal.swd")

!sed -i 's/_random//g' /opt/src/agder_eel/aal.swd

# Re-run reference maxent model with recent data and human influenced parametrs added
!printf "kulvert_helning_nedstroems_03_max\nkulvert_helning_nedstroems_09_max\nkulvert_helning_nedstroems_21_max\nkulvert_helning_nedstroems_31_max\nkulvert_helning_nedstroems_51_max" | awk '{print("java -mx351024m -jar /opt/src/maxent.jar nowarnings noprefixes responsecurves jackknife outputfiletype=mxe outputdirectory=/opt/src/maxent_results_" $1 " samplesfile=/opt/src/agder_eel/aal_abs.swd environmentallayers=/opt/src/agder_eel/bgr_abs.swd maximumbackground=100000 replicates=1 writebackgroundpredictions noaddsamplestobackground writeplotdata nooutputgrids appendtoresultsfile threads=1 verbose -a -r warnings=False -N helning_nedstroems_03_max -N helning_nedstroems_09_max -N helning_nedstroems_21_max -N helning_nedstroems_31_max -N helning_nedstroems_51_max -N helning_nedstroems_21_max -N avstand_hoh -N innsjoe_oppstroems_antall -N kulvert_helning_nedstroems_03_max -N kulvert_helning_nedstroems_09_max -N kulvert_helning_nedstroems_21_max -N kulvert_helning_nedstroems_31_max -N kulvert_helning_nedstroems_51_max -N " $1 " -N spi -N spi_akkumulert -N overvannsskkumulering -E aal -t innsjoe_oppstroems \0")}' | xargs -0 -P3 -I{} bash -c "{}"

!r.maxent.lambdas --o --v -nc lambdas_file=/opt/src/maxent_results_helning_nedstroems_21_max_final/aal_0.lambdas alias_file=/opt/src/agder_eel/aal_alias.txt raw=aal_habitat_historisk nprocs=30 width=62150 height=3700
!r.maxent.lambdas --o --v -nc lambdas_file=/opt/src/maxent_results_kulvert_helning_nedstroems_31_max/aal_etter_1980.lambdas alias_file=/opt/src/agder_eel/aal_alias.txt raw=aal_habitat_etter_1980 nprocs=30 width=62150 height=3700
!r.maxent.lambdas --o --v -nc lambdas_file=/opt/src/maxent_results_kulvert_helning_nedstroems_31_max/aal_etter_2000.lambdas alias_file=/opt/src/agder_eel/aal_alias.txt raw=aal_habitat_etter_2000 nprocs=30 width=62150 height=3700

!v.rast.stats -c -d --verbose map=dem_1m_carved_streams_cleaned_full_network_cat2 layer=5 raster=aal_habitat_historisk,aal_habitat_etter_1980,aal_habitat_etter_2000 column_prefix=aal_habitat_historisk,aal_habitat_etter_1980,aal_habitat_etter_2000 method=average
!v.what.rast -i --verbose map=dem_1m_carved_streams_cleaned_full_network_cat2 layer=4 raster=aal_habitat_historisk column=aal_habitat_historisk
!v.what.rast -i --verbose map=dem_1m_carved_streams_cleaned_full_network_cat2 layer=4 raster=aal_habitat_etter_1980 column=aal_habitat_etter_1980
!v.what.rast -i --verbose map=dem_1m_carved_streams_cleaned_full_network_cat2 layer=4 raster=aal_habitat_etter_2000 column=aal_habitat_etter_2000


    gscript.run_command("v.db.addcolumn", map="{}_cat2".format(full_network), layer="5", columns="aal_habitat_tapt_1980 DOUBLE PRECISION, aal_habitat_tapt_2000 DOUBLE PRECISION")

    gscript.run_command("v.db.update", map="{}_cat2".format(full_network), layer="5", column="aal_habitat_tapt_1980",  query_column='aal_habitat_historisk_average-aal_habitat_etter_1980_average')
    gscript.run_command("v.db.update", map="{}_cat2".format(full_network), layer="5", column="aal_habitat_tapt_2000",  query_column='aal_habitat_historisk_average-aal_habitat_etter_2000_average')


    net = (
        gscript.read_command(
            "v.net",
            input="{}_cat2".format(full_network),
            points="{}_cat2".format(full_network),
            node_layer=4,
            arc_layer=5,
            operation="report",
            quiet=True,
        )
        .rstrip("\n")
        .split("\n")
    )


    edge_attrs = np.genfromtxt(
        #BytesIO(
            gscript.read_command(
                "v.db.select", map="{}_cat2".format(full_network), layer="5", separator="|", quiet=True
            )
            .rstrip("\n").split("\n"),
            #.encode()
        #),
        dtype=None,
        names=True,
        delimiter="|",
    )

    # Parse network data and extract vertices, edges and edge names
    edges = []
    vertices = []
    edge_cat = []
    for l in net:
        if l != "":
            # Names for edges and vertices have to be of type string
            # Names (cat) for edges
            edge_cat.append(l.split(" ")[0])

            # From- and to-vertices for edges
            edges.append((l.split(" ")[1], l.split(" ")[2]))

            # Names (cat) for from-vertices
            vertices.append(l.split(" ")[1])

            # Names (cat) for to-vertices
            vertices.append(l.split(" ")[2])

    # Create Graph object
    g = Graph().as_directed()

    # Add vertices with names
    vertices.sort()
    vertices = set(vertices)
    g.add_vertices(list(vertices))

    # Add edges with names
    g.add_edges(edges)
    g.es["cat"] = edge_cat

    # Add edge attributes
    for colname in ['length_m', 'aal_habitat_historisk_average',
 'aal_habitat_etter_1980_average',
 'aal_habitat_etter_2000_average']:  # edge_attrs.dtype.names[1:]
        g.es[colname] = edge_attrs[colname]

    culvert_vertices = gscript.read_command(
            "v.db.select", map="{}_cat2".format(full_network), layer="4", separator="|", columns="cat", where="culvert>0", quiet=True
        ).rstrip("\n").split("\n")
    g.vs["culvert"] = 0
    g.vs.select(name_in=culvert_vertices)["culvert"] = 1

    habitat_columns = ["aal_habitat_historisk_oppstroems", "aal_habitat_tapt_1980_oppstroems", "aal_habitat_tapt_2000_oppstroems", "aal_habitat_historisk_oppstroems_uniq", "aal_habitat_tapt_1980_oppstroems_uniq", "aal_habitat_tapt_2000_oppstroems_uniq"]

    gscript.run_command("v.db.addcolumn", map="{}_cat2".format(full_network), layer="4", columns=", ".join(["{} DOUBLE PRECISION".format(a) for a in habitat_columns]))

    for col in habitat_columns:
        gscript.run_command("v.db.update", map="{}_cat2".format(full_network), layer="4", column=col, value=0)

    update_sql = ""
    for vertex in g.vs.select(culvert_eq=1):
        value_dict = {}
        # Upstream graph
        us_g = g.subgraph(sorted([
                        bfs_node.index
                        for bfs_node in g.bfsiter(vertex, mode="IN")
                    ] + [vertex.index]))

        if len(us_g.es) > 0:
            value_dict["aal_habitat_historisk_oppstroems"] = np.nansum(np.array(us_g.es['length_m']) * np.array(us_g.es['aal_habitat_historisk_average']))
            value_dict["aal_habitat_tapt_1980_oppstroems"] = np.nansum(np.array(us_g.es['length_m']) * (np.array(us_g.es['aal_habitat_historisk_average']) - np.array(us_g.es['aal_habitat_etter_1980_average'])))
            value_dict["aal_habitat_tapt_2000_oppstroems"] = np.nansum(np.array(us_g.es['length_m']) * (np.array(us_g.es['aal_habitat_historisk_average']) - np.array(us_g.es['aal_habitat_etter_2000_average'])))

            upstream_culverts = us_g.vs.select(culvert=1).select(name_ne=vertex["name"])

            if len(upstream_culverts) > 0:
                us_g.delete_edges(list(itertools.chain(*[c.in_edges() for c in upstream_culverts])))

            if len(us_g.es) > 0:
                us_es = us_g.es.select(
                        _within=[
                            bfs_node.index
                            for bfs_node in list(
                                us_g.bfsiter(
                                    us_g.vs.select(
                                        name_eq=vertex["name"]
                                    )[0],
                                    mode="IN",
                                )
                            )
                        ]
                    )
                value_dict["aal_habitat_historisk_oppstroems_uniq"] = np.nansum(np.array(us_es['length_m']) * np.array(us_es['aal_habitat_historisk_average']))
                value_dict["aal_habitat_tapt_1980_oppstroems_uniq"] = np.nansum(np.array(us_es['length_m']) * (np.array(us_es['aal_habitat_historisk_average']) - np.array(us_es['aal_habitat_etter_1980_average'])))
                value_dict["aal_habitat_tapt_2000_oppstroems_uniq"] = np.nansum(np.array(us_es['length_m']) * (np.array(us_es['aal_habitat_historisk_average']) - np.array(us_es['aal_habitat_etter_2000_average'])))
            else:
                value_dict["aal_habitat_historisk_oppstroems_uniq"] = value_dict["aal_habitat_historisk_oppstroems"]
                value_dict["aal_habitat_tapt_1980_oppstroems_uniq"] = value_dict["aal_habitat_tapt_1980_oppstroems"]
                value_dict["aal_habitat_tapt_2000_oppstroems_uniq"] = value_dict["aal_habitat_tapt_2000_oppstroems"]
        else:
            continue
        # Update values
        update_sql += "UPDATE {table} SET {col_val} WHERE cat = {cat};\n".format(table=table, cat=vertex["name"], col_val=", ".join([" = ".join([col, str(val) if val else "NULL"]) for col, val in value_dict.items()]))


    with open("/opt/src/agder_eel/sqlfile.sql", mode="w") as sqlfile:
        sqlfile.write(update_sql)
    gscript.run_command("db.execute", input="/opt/src/agder_eel/sqlfile.sql")



    from datetime import date
    geopackage = "/home/stefan/agder_eel_net_{}.gpkg".format(
        date.today().strftime("%Y_%m_%d")
    )

    gscript.run_command(
        "v.out.ogr",
        overwrite=True,
        verbose=True,
        input="{}_cat2".format(full_network),
        output=geopackage,
        format="GPKG",
        output_layer="nodes",
        type="point",
        layer=export_node_layer,
    )

    gscript.run_command(
        "v.out.ogr",
        flags="u",
        overwrite=True,
        verbose=True,
        input="{}_cat2".format(full_network),
        output=geopackage,
        format="GPKG",
        output_layer="network",
        type="line",
        layer=export_edge_layer,
    )

    gscript.run_command(
        "v.out.ogr",
        flags="u",
        overwrite=True,
        verbose=True,
        input="{}_cat2".format(full_network),
        output=geopackage,
        format="GPKG",
        output_layer="network_2",
        type="line",
        layer="3",
    )

    gscript.run_command(
        "v.out.ogr",
        flags="u",
        overwrite=True,
        verbose=True,
        input="{}_cat2".format(full_network),
        output=geopackage,
        format="GPKG",
        output_layer="nodes_2",
        type="point",
        layer="2",
    )




import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

bgr = pd.read_csv('/opt/src/agder_eel/bgr.swd')
bgr["overvannsskkumulering_abs"] =  bgr["overvannsskkumulering"].abs()
bgr.to_csv('/opt/src/agder_eel/bgr_abs.swd')
corr = bgr.corr()
plt.matshow(corr)

ax = sns.heatmap(corr, vmin=-1, vmax=1, center=0, cmap=sns.diverging_palette(20,220,n=200), annot=True)
ax.set_xticklabels(ax.get_xticklabels(), rotation=45, horizontalalignment="right")
plt.show()




import pandas as pd
import seaborn as sns

bgr = pd.read_csv('/opt/src/agder_eel/bgr.swd')
corr = bgr.corr()
ax = sns.heatmap(corr, vmin=-1, vmax=1, center=0, cmap=sns.diverging_palette(20,220,n=200))

ax.set_xticklabels(ax.get_xticklabels(),
rotation=45,
horizontalalignment="right")
