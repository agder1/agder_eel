#!/bin/bash

g.extension extension=r.hydrodem
g.extension extension=r.stream.culvert url=https://gitlab.com/invafish/r.stream.culvert

#! /bin/bash

gdalbuildvrt /home/shared/data/agder_eel/dem_1m/dem_1m.vrt /home/shared/data/agder_eel/dem_1m/*.tif

r.in.gdal --overwrite --verbose input="/home/shared/data/agder_eel/dem_1m/dem_1m.vrt" output="dem_1m" memory=2047 offset=0 num_digits=0

g.region n=6559355.00012207 s=6449874.00012207 w=17575 e=79725 res=1 -p

r.fillnulls --overwrite --verbose input="dem_1m" output="dem_1m_fillnulls" method="bilinear" memory=30000

r.hydrodem -af input=dem_1m_fillnulls memory=90000 output=dem_1m_filled --overwrite --verbose
# r.terraflow --overwrite --verbose elevation="dem_1m_fillnulls" filled="dem_1m_filled" memory=32000 directory="/home/shared/data/agder_eel/terraflow" stats="/home/shared/data/agder_eel/terraflow_stats.txt"

v.in.ogr --o --v -r encoding=UTF8 layer=Topography.Norway_FKB_Bane_lines input="PG:dbname=gisdata host=gisdata-db.nina.no" output=Norway_FKB_Bane_lines --o

for layer in Norway_FKB_TraktorvegSti_lines Norway_FKB_Veg_lines Norway_FKB_Veg_polygons Norway_FKB_Vann_polygons Norway_FKB_Vann_lines Norway_N50_VegSti Norway_N50_Bane Norway_N50_ElvBekk
do
v.in.ogr --o --v -r layer=Topography.$layer input="PG:dbname=gisdata host=gisdata-db.nina.no" output=$layer
done

v.in.ogr --o --v -r layer=Topography.Norway_FKB_BygnAnlegg_lines input="PG:dbname=gisdata host=gisdata-db.nina.no" output=Norway_FKB_BygnAnlegg_lines where="objekttypenavn IN ('Stikkrenne', 'Rørgate', 'Kulvert', 'Fløtningsrenne', 'Fisketrapp', 'FiskehjellGrense', 'FiskehjellMøne', 'ElveterskelKant', 'Elveforbygningskant', 'Damkant', 'Brudetalj')"
#v.in.ogr --o --v -r layer=Topography.Norway_FKB_AR5_polygons input="PG:dbname=gisdata host=gisdata-db.nina.no" output=Norway_FKB_Hav where="\"arealressursArealtype\"=82"
v.in.ogr --o --v -r layer=Topography.Norway_N50_ArealdekkeFlate input="PG:dbname=gisdata host=gisdata-db.nina.no" output=Norway_N50_ArealdekkeFlate where="\"OBJTYPE\"='Innsjø'"
#v.in.ogr --o --v -r layer=Topography.Norway_N50_ArealdekkeFlate input="PG:dbname=gisdata host=gisdata-db.nina.no" output=Norway_N50_Vann where="\"OBJTYPE\" IN ('Innsjø', 'Hav', 'ElvBekk')"

# v.voronoi is slow
# v.db.addcolumn map=Norway_FKB_Veg_polygons@p_eel_FKB_N50 columns="id smallint"
# v.db.update map=Norway_FKB_Veg_polygons@p_eel_FKB_N50 column=id value=1
# v.dissolve --overwrite --verbose input=Norway_FKB_Veg_polygons@p_eel_FKB_N50 column=id output=Norway_FKB_Veg_polygons_dissolved

for m in Elvenett DamLinje DamPunkt Vannvei Vannkraftverk RegineEnhet NedborfeltTilHav
do
    v.in.ogr --o --v -r layer=Hydrography.Norway_NVE_$m input="PG:dbname=gisdata host=gisdata-db.nina.no" output=Norway_NVE_$m
done

for m in $(g.list -r type=vector pattern="Norway_FKB_.*eg.*\|Norway_FKB_Bane.*\|Norway_FKB_Vann*\|Norway_N50_*") #Norway_NVE_DamLinje Norway_NVE_DamPunkt Norway_NVE_Vannvei Norway_NVE_Vannkraftverk Norway_NVE_RegineEnhet Norway_NVE_NedborfeltTilHav #$(g.list type=vector)
do
    case "$m" in
        Norway_NVE_RegineEnhet|Norway_NVE_NedborfeltTilHav) use=cat;;
        *)                                                  use=val;;
    esac

    v.to.rast input=$m output=$m use=$use memory=50000 --o --v
done

# The following could be done faster and with less waste of storage using r.buildvrt
r.patch --o --v input=$(g.list -r type=vector pattern="Norway_FKB_.*eg.*\|Norway_FKB_Bane.*" separator=',') output=FKB_Barriers
r.patch --o --v input=$(g.list -r type=vector pattern="Norway_FKB_Vann.*" separator=',') output=FKB_Vann

r.patch --o --v input=Norway_N50_VegSti,Norway_N50_Bane output=N50_Barriers
r.patch --o --v input=FKB_Barriers,N50_Barriers output=FKB_N50_Barriers

g.mapsets operation=add mapset=p_eel_FKB_N50
r.thin --o --v input=FKB_N50_Barriers output=FKB_N50_Barriers_thin

v.extract --overwrite --verbose input=Norway_FKB_Vann_polygons output=Norway_FKB_Hav where=objekttypenavn = 'Havflate'
v.extract -d -t --overwrite --verbose input=Norway_FKB_Vann_polygons where="objekttypenavn = 'Innsjø'" output=Norway_FKB_Vann_innsjoe dissolve_column=objekttypenavn
v.to.rast --overwrite --verbose input=Norway_FKB_Hav output=Norway_FKB_Hav use=val memory=25000 &
#v.to.rast --overwrite --verbose input=Norway_FKB_Vann_polygons output=Norway_FKB_Innsjoe use=val where="objekttypenavn = 'Innsjø'" memory=25000 &
#v.to.rast --overwrite --verbose input=Norway_N50_ArealdekkeFlate  output=Norway_N50_Innsjoe use=val where="objekttypenavn = 'Innsjø'" memory=25000 &

#v.to.rast --overwrite --verbose input=Norway_NVE_Elvenett@p_eel_FKB_N50 output=Norway_NVE_Elvenett use=cat memory=30000
# r.grow.distance -m --overwrite --verbose input=dem_1m_carved_streams_thin@p_eel_test distance=dem_1m_carved_streams_thin_dist

v.in.nvdb.py --overwrite --verbose datatype=supplemental_data nvdb_api_path=/home/stefan/nvdbapi-V3 objects=60,78,79 contact=stefan.blumentrath@nina.no maxcount=0 output_prefix=NVDB

for nve_map in DamLinje DamPunkt Vannkraftverk
do
v.in.ogr -2 input=/home/shared/data/agder_eel/NVEData/Vannkraft/Vannkraft_${nve_map}.shp output=Norway_NVE_Vannkraft_${nve_map} --o --v
done

r.geomorphon --overwrite -e elevation="dem_1m_fillnulls" forms="dem_1m_fillnulls_forms" search=13 skip=5 flat=3 dist=10 step=0 start=0
r.stream.culvert elevation=dem_1m_fillnulls culverts=culverts channels=dem_1m_fillnulls_forms memory=90000 filled_dem=dem_1m_filled barriers=FKB_N50_Barriers_thin streams=FKB_Vann@p_eel_FKB_N50 waterbodies=Norway_FKB_Vann_innsjoe@p_eel_FKB_N50 --v --o


# g.region raster=dem_1m_fillnulls &&

# Single quotes needed cause with quotes, ! is treated as an event
r.mapcalc --overwrite --verbose expression='MASK=if(isnull(Norway_FKB_Hav),1,if(Norway_FKB_Hav, \
if(isnull(Norway_FKB_Hav[-1,-1])&&!isnull(dem_1m_fillnulls[-1,-1]), 1, \
if(isnull(Norway_FKB_Hav[-1,0])&&!isnull(dem_1m_fillnulls[-1,0]), 1, \
if(isnull(Norway_FKB_Hav[-1,1])&&!isnull(dem_1m_fillnulls[-1,1]), 1, \
if(isnull(Norway_FKB_Hav[0,1])&&!isnull(dem_1m_fillnulls[0,1]), 1, \
if(isnull(Norway_FKB_Hav[0,-1])&&!isnull(dem_1m_fillnulls[0,-1]), 1, \
if(isnull(Norway_FKB_Hav[1,-1])&&!isnull(dem_1m_fillnulls[1,-1]), 1, \
if(isnull(Norway_FKB_Hav[1,0])&&!isnull(dem_1m_fillnulls[1,0]), 1, \
if(isnull(Norway_FKB_Hav[1,1])&&!isnull(dem_1m_fillnulls[1,1]), 1, \
null())))))))), 1))' &&

r.stream.carve --overwrite --verbose input=culverts where="culvert_rank=1" elevation=dem_1m_fillnulls memory=50000 output=dem_1m_carved

r.stream.extract --overwrite --verbose elevation=dem_1m_carved threshold=5000 stream_length=100 memory=90000 stream_raster=dem_1m_carved_streams stream_vector=dem_1m_carved_streams direction=dem_1m_carved_fdir

v.clean input=dem_1m_carved_streams output=dem_1m_carved_streams_cleaned type=line error=r_stream_extract_errors tool=snap,break,rmdupl layer=1 threshold=0.25


# r.thin --overwrite --verbose input=dem_1m_carved_streams@p_eel_test output=dem_1m_carved_streams_thin
# r.to.vect -v --overwrite --verbose input=dem_1m_carved_streams_thin@p_eel_test output=dem_1m_carved_streams type=line

r.watershed -m -b --overwrite --verbose elevation=dem_1m_carved threshold=1000 accumulation=dem_1m_carved_flow_accum tci=dem_1m_carve_tci spi=dem_1m_carved_SPI basin=dem_1m_carved_basins memory=40000 &
r.stream.basins -l --overwrite --verbose direction=dem_1m_carved_fdir stream_rast=dem_1m_carved_streams memory=40000 basins=dem_1m_carved_basins_ocean &
r.slope.direction --overwrite --verbose elevation=dem_1m_carved direction=dem_1m_carved_fdir dir_type=auto steps=3,9,21,31,51 slope_measure=degree_int output=dem_1m_carved_slope_03,dem_1m_carved_slope_09,dem_1m_carved_slope_21,dem_1m_carved_slope_31,dem_1m_carved_slope_51

v.overlay --overwrite --verbose ainput=Norway_FKB_TraktorvegSti_lines atype=line binput=Norway_FKB_Veg_polygons operator=not output=Norway_FKB_TraktorvegSti_lines_np

v.in.pygbif --overwrite --verbose output=eel taxa="anguilla anguilla" rank=species

r.mask --v --o raster=dem_1m_carved_streams
r.mapcalc --overwrite --verbose expression="dem_1m_carved_SPI_filled=if(isnull(dem_1m_carved_SPI), if(dem_1m_carved_streams, nmedian(dem_1m_carved_SPI[0,1], dem_1m_carved_SPI[1,0], dem_1m_carved_SPI[0,-1], dem_1m_carved_SPI[-1,0], dem_1m_carved_SPI[-1,-1], dem_1m_carved_SPI[1,-1], dem_1m_carved_SPI[-1,1], dem_1m_carved_SPI[1,1]), null()), dem_1m_carved_SPI)" &&
r.cost -n --overwrite --verbose input=dem_1m_carved_SPI_filled output=dem_1m_carved_cost_SPI start_raster=Norway_FKB_Hav@p_eel_test memory=80000 &
#r.cost -n --overwrite --verbose input=MASK output=hav_cost_dist outdir=upstreams_dir start_raster=Norway_FKB_Hav@p_eel_test memory=100000
r.stream.distance --overwrite --verbose -o stream_rast="dem_1m_carved_streams" direction="dem_1m_carved_fdir" elevation="dem_1m_carved" method="downstream" distance="dem_1m_carved_streams_distance" difference="dem_1m_carved_streams_elev_distance" memory=25000




v.to.rast -d --overwrite --verbose input=dem_1m_carved_streams_full_network type=line output=dem_1m_carved_streams_full_network_culverts_n use=attr attribute_column=downstream_culverts_n memory=3000
for scale in 03 09 21 31 51
do
    v.to.rast -d --overwrite --verbose input=dem_1m_carved_streams_full_network type=line output=dem_1m_carved_streams_full_network_downstream_slope_${scale}_max use=attr attribute_column=downstream_dem_1m_carved_slope_${scale}_maximum memory=3000
done

for scale in ['03', '09', '21', '31', '51']:
    gscript.run_command("v.to.rast", flags="d", overwrite=True, verbose=True, input="dem_1m_carved_streams_full_network", type="line", output="dem_1m_carved_streams_full_network_downstream_slope_{}_max".format(scale), use="attr", attribute_column="downstream_dem_1m_carved_slope_{}_maximum".format(scale), memory=3000)
